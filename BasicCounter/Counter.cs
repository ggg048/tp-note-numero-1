﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class Counter
    {
        public int valeur;
       


        public Counter(int valeur)
        {
            this.valeur = valeur;
        }

        public void increment()
        {
            this.valeur = this.valeur + 1;
        }

        public void decrement()
        {
            if (this.valeur !=0)
            {
                this.valeur = this.valeur - 1;
            }
            
        }

        public void remiseazero()
        {
            this.valeur = 0; 
        }

        public int getValeur()
        {
            return this.valeur;
        }
    }
}
