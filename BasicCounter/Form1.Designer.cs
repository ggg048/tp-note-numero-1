﻿namespace BasicCounter
{
    partial class BasicCounter
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bouton_moin = new System.Windows.Forms.Button();
            this.button_plus = new System.Windows.Forms.Button();
            this.Affichage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_reset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bouton_moin
            // 
            this.bouton_moin.Location = new System.Drawing.Point(172, 161);
            this.bouton_moin.Name = "bouton_moin";
            this.bouton_moin.Size = new System.Drawing.Size(75, 23);
            this.bouton_moin.TabIndex = 1;
            this.bouton_moin.Text = "-";
            this.bouton_moin.UseVisualStyleBackColor = true;
            this.bouton_moin.Click += new System.EventHandler(this.button_moin);
            // 
            // button_plus
            // 
            this.button_plus.Location = new System.Drawing.Point(510, 161);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(75, 23);
            this.button_plus.TabIndex = 2;
            this.button_plus.Text = "+";
            this.button_plus.UseVisualStyleBackColor = true;
            this.button_plus.Click += new System.EventHandler(this.button_Plus);
            // 
            // Affichage
            // 
            this.Affichage.AutoSize = true;
            this.Affichage.Location = new System.Drawing.Point(357, 166);
            this.Affichage.Name = "Affichage";
            this.Affichage.Size = new System.Drawing.Size(35, 13);
            this.Affichage.TabIndex = 3;
            this.Affichage.Text = "label1";
            this.Affichage.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(357, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // button_reset
            // 
            this.button_reset.Location = new System.Drawing.Point(340, 225);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(75, 23);
            this.button_reset.TabIndex = 5;
            this.button_reset.Text = "Reset";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Click += new System.EventHandler(this.bouton_reset);
            // 
            // BasicCounter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Affichage);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.bouton_moin);
            this.Name = "BasicCounter";
            this.Text = "BasicCounter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bouton_moin;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Label Affichage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_reset;
    }
}

