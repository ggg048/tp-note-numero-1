﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounter;
namespace BasicCounterTest
{
    [TestClass]
    public class TestCounter
    {
        [TestMethod]
        public void TestMethodincrement()
        {
            Counter valeur = new Counter(0);

            valeur.increment();

            Assert.AreEqual(1, valeur.getValeur());
        }

        [TestMethod]
        public void TestMethoddecrement()
        {
            Counter valeur = new Counter(1);

            valeur.decrement();

            Assert.AreEqual(0, valeur.getValeur());
        }

        [TestMethod]
        public void Testremiseazero()
        {
            Counter valeur = new Counter(0);
            valeur.remiseazero();
            Assert.AreEqual(0, valeur.getValeur());
        }

        [TestMethod]
        public void TestgetValeur()
        {
            Counter valeur = new Counter(0);
            valeur.getValeur();
            Assert.AreEqual(0, valeur.getValeur());
        }

    }
}
